#!/bin/bash

if ! [ -x "$(command -v florence)" ]; then
	echo run 'sudo dnf install florence' first
	exit 1
fi

sudo \cp -f florence.conf /usr/share/florence/

createLauncher() {
	p=/home/exteremist/.config/xfce4/panel/launcher-$1
	mkdir -p $p
	\cp -f $2.desktop $p
	xfconf-query -c xfce4-panel -p /plugins/plugin-$1 -n -t string -s "launcher"
	xfconf-query -c xfce4-panel -p /plugins/plugin-$1/items -n -t string -s "$2.desktop" -a
}
createLauncher 14 zoom-in
createLauncher 16 zoom-out
createLauncher 17 florence

xfconf-query -c xfce4-panel -p /panels/panel-1/plugin-ids -rR
xfconf-query -c xfce4-panel -p /panels/panel-1/plugin-ids -n \
        -t int -t int -t int -t int -t int -t int -t int -t int -t int -t int -t int -t int -t int \
        -s 1   -s 14  -s 16  -s 17  -s 8   -s 3   -s 15  -s 4   -s 5   -s 7   -s 6   -s 2   -s 9

xfce4-panel -r

#!/bin/bash

removeLauncher() {
	\rm -rf /home/exteremist/.config/xfce4/panel/launcher-$1
	xfconf-query -c xfce4-panel -p /plugins/plugin-$1 -rR
}

removelauncher 14
removelauncher 16
removelauncher 17

xfconf-query -c xfce4-panel -p /panels/panel-1/plugin-ids -rR
xfconf-query -c xfce4-panel -p /panels/panel-1/plugin-ids -n \
	-t int -t int -t int -t int -t int -t int -t int -t int -t int -t int \
	-s 1   -s 8   -s 3   -s 15  -s 4   -s 5   -s 7   -s 6   -s 2   -s 9

xfce4-panel -r

if [ -x "$(command -v florence)" ]; then
	echo florence virtual keyboard is installed
fi

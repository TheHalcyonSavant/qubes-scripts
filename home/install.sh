#!/bin/bash

if [ ! -d bak ]; then
	echo Backing up .bashrc ...
	mkdir bak bak/exteremist bak/root
	cp /home/exteremist/.bashrc bak/exteremist/.bashrc
	cp /home/exteremist/.bash_profile bak/exteremist/.bash_profile
	sudo cp /root/.bashrc bak/root/.bashrc
	sudo cp /root/.bash_profile bak/root/.bash_profile
fi

\cp -f bashrc /home/exteremist/.bashrc
\cp -f bash_profile /home/exteremist/.bash_profile
sudo \cp -f bashrc /root/.bashrc
sudo \cp -f bash_profile /root/.bash_profile
echo .bashrc and .bash_profile applied successfully

#!/bin/bash

if [ ! -d bak ]; then
	echo "Missing backup (bak) folder!"
	exit 2
fi

\cp -f bak/exteremist/.bashrc /home/exteremist/.bashrc
\cp -f bak/exteremist/.bash_profile /home/exteremist/.bash_profile
sudo \cp -f bak/root/.bashrc /root/.bashrc
sudo \cp -f bak/root/.bash_profile /root/.bash_profile
echo .bashrc and .bash_profile restored successfully

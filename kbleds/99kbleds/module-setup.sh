#!/bin/bash

check() {
  return 0
}

depends() {
  echo kernel-modules
}

install() {
  inst_hook pre-trigger 99 "$moddir/kbleds-forked.sh"
}

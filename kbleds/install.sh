#!/bin/bash

echo Installing kbleds.sh...
sudo \cp -f kbleds.sh /usr/lib/kbleds.sh

echo Installing xfce startup script...
mkdir -p /home/exteremist/.config/autostart
\cp -f auto-kbd-light.desktop /home/exteremist/.config/autostart/auto-kbd-light.desktop

echo Installing udev kbleds.rules...
sudo \cp -f kbleds.rules /etc/udev/rules.d/kbleds.rules
sudo udevadm control -R

echo Installing kbleds.service...
sudo \cp -f kbleds.service /etc/systemd/system/kbleds.service
systemctl daemon-reload
systemctl restart kbleds

echo Packaging dracut module...
sudo \cp -rTf 99kbleds/ /usr/lib/dracut/modules.d/99kbleds/
sudo dracut -fv --install "/bin/tee /usr/lib/kbleds.sh" --add kbleds
	#--add-drivers input_leds \   # apparently this line is not needed for newer linux kernels
initrm=initramfs-`uname -r`.img
efipath=/boot/efi/EFI/qubes
if ! sudo test -f $efipath/$initrm.bak; then
	echo Making backup of $initrm...
	sudo cp $efipath/$initrm $efipath/$initrm.bak
fi
sudo \cp -f /boot/$initrm $efipath/$initrm

echo Installation finished successfully
symbols=/usr/share/X11/xkb/symbols
if [[ $(diff us $symbols/us) > 0 ]]; then
        echo WARNING: us != $symbols
	diff us $symbols/us
        echo WARNING: copy us to $symbols manually:
	echo sudo cp us $symbols/us
fi

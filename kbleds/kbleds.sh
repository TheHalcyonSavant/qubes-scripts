#!/bin/bash

#set -x

sudo() {
	[[ $EUID = 0 ]] || set -- command sudo "$@"
	"$@"
}

file=/sys/class/leds/input*scroll*/brightness
for ((ret=0; ret<10; ret++)); do
	echo 'loop'
	sleep 2
	if [ -f $file ]; then
                echo 1 | sudo tee $(ls $file)
		echo 'brightness changed'
		break
        fi
done

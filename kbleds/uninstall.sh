#!/bin/bash

echo Uninstalling kbleds dracut module...
sudo \rm -rf /usr/lib/dracut/modules.d/99kbleds
sudo dracut -fv
initrm=initramfs-`uname -r`.img
sudo \cp -f /boot/$initrm /boot/efi/EFI/qubes/$initrm

echo Uninstalling xfce startup script...
\rm -f /home/exteremist/.config/autostart/auto-kbd-light.desktop

echo Uninstalling kbleds.service...
sudo \rm -f /etc/systemd/system/kbleds.service
systemctl daemon-reload

echo Uninstalling udev kbleds.rules...
sudo \rm -f /etc/udev/rules.d/kbleds.rules
sudo udevadm control -R

echo Removal finished successfully

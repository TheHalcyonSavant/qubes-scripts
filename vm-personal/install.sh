#!/bin/bash

qvm-start --skip-if-running personal
if ! qvm-block | grep -q personal; then
	qvm-block a -p personal `qvm-block | grep Data | awk '{print $1}'`
	qvm-block a -p personal `qvm-block | grep Win10 | awk '{print $1}'`
fi
cat rc.local.sh | qvm-run -p personal 'cat | sudo tee /rw/config/rc.local' > /dev/null
cat user-dirs.dirs | qvm-run -p personal 'cat | sudo tee /rw/config/user-dirs.dirs' > /dev/null
qvm-run -p personal "sudo chmod +x /rw/config/rc.local"
sudo qvm-shutdown --wait personal

#!/bin/bash

qvm-run -p personal "rm -rf /home/user/QubesIncoming"
qvm-copy-to-vm personal bak/*
qvm-run -p personal "mountpoint -q /media/Data && sudo umount /media/Data;"\
"mountpoint -q /media/Win10 && sudo umount /media/Win10;"\
"sudo cp -f /home/user/QubesIncoming/dom0/rc.local.sh /rw/config/rc.local;"\
"sudo chmod -x /rw/config/rc.local;"\
"cp -f /home/user/QubesIncoming/dom0/user-dirs.dirs /home/user/.config/user-dirs.dirs;"\
"sudo cp -f /home/user/QubesIncoming/dom0/user-dirs.dirs /rw/home/user/.config/user-dirs.dirs"

if qvm-block | grep -q personal; then
	while qvm-check --running personal > /dev/null; do
		sudo qvm-shutdown --wait personal
		sleep 2
	done
	qvm-block d personal `qvm-block | grep Data | awk '{print $1}'`
	qvm-block d personal `qvm-block | grep Win10 | awk '{print $1}'`
fi
